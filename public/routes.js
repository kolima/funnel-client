import React from 'react';
import { Route } from 'react-router';
import NoMatchPage from './containers/NoMatchPage';
import Spinner from './containers/Spinner';
import LoginPage from './containers/LoginPage';
import ForgotPassword from './containers/ForgotPassword';
import CreatePasswordPage from './containers/CreatePasswordPage';
import EditPasswordPage from './containers/EditPasswordPage';
import CreateCardPage from './containers/CreateCardPage';
import ListDeskCardsPage from './containers/ListDeskCardsPage';
import App from './containers/App';
import Dashboard from './containers/DashboardPage';

import Acl from './helpers/acl';

export default (serverCookies, store) => {
  return (
    <Route component={Spinner}>
      <Route onEnter={Acl.login(store, serverCookies)}>
        <Route path="/login" component={LoginPage}/>
        <Route path='/password/forgot' component={ForgotPassword}/>
        <Route path="/password/create" component={CreatePasswordPage}/>
        <Route path="/password/edit" component={EditPasswordPage}/>
      </Route>

      <Route component={App} onEnter={Acl.app(store, serverCookies)}>
        <Route path="/" component={Dashboard}/>
        <Route path="/cards/create" component={CreateCardPage}/>
        <Route path="/desks/:desk/cards" component={ListDeskCardsPage}/>
      </Route>

      <Route path="*" component={NoMatchPage}/>
    </Route>
  );
};

