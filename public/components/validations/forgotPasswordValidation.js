import { createValidator, required, email, maxLength } from 'funnel-frontend-core/helpers/validation';

const forgotPasswordValidation = createValidator({
  email: [required, email, maxLength(255)]
});

export default forgotPasswordValidation;
