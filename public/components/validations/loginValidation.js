import { createValidator, required, email, minLength, maxLength } from 'funnel-frontend-core/helpers/validation';

const loginValidation = createValidator({
  email : [required, email, maxLength(255)],
  password: [required, minLength(2), maxLength(30)]
});

export default loginValidation;
