import { createValidator, required, maxLength, email } from 'funnel-frontend-core/helpers/validation';

const cardValidation = createValidator({
  translation : [required, maxLength(255)],
  original: [required, maxLength(255)]
});

export default cardValidation;
