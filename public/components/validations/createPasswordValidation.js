import { createValidator, required, minLength, maxLength, email, equal } from 'funnel-frontend-core/helpers/validation';

const createPasswordValidation = createValidator({
  password : [required, minLength(2), maxLength(30)],
  confirmPassword: [required, minLength(2), maxLength(30), equal('password')]
});

export default createPasswordValidation;
