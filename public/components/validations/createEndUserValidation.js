import { createValidator, required, maxLength, email } from 'funnel-frontend-core/helpers/validation';

const createEndUserValidation = createValidator({
  userName : [required, maxLength(255)],
  userEmail: [required, email, maxLength(255)]
});

export default createEndUserValidation;
