import React, { Component, PropTypes} from 'react';
import { Panel, Input, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { connectReduxForm, focus, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import * as userActions from '../../redux/modules/users';
import { setFieldDecor } from 'funnel-frontend-core/helpers/fieldStyle';
import createPasswordValidation from '../../components/validations/createPasswordValidation';
import * as alertBarActions from 'funnel-frontend-core/modules/alert';
import AlertBar from 'funnel-frontend-core/components/AlertBar/ErrorAlertBar';
import * as authActions from '../../redux/modules/auth';

let initFields = [
  'password',
  'confirmPassword'
];

@connect(
  state => ({
    isErrorVisible: state.auth.isErrorVisible,
    password: state.auth.password
  }),
  dispatch => bindActionCreators({...userActions, ...alertBarActions, ...authActions}, dispatch)
)

@connectReduxForm({
  form: 'create-password',
  fields: initFields,
  validate: createPasswordValidation
})

export default
class PasswordForm extends Component {
  constructor(props) {
    super(props);
  }

  componentWillUpdate() {
    if (this.props.isErrorVisible) {
      this.props.cleanPasswordErrors();
    }
  }

  showValidationErrors() {
    window.scrollTo(0, 0);
    this.props.showErrorAlert();
  }

  render() {
    const { onSubmit, fields: {password, confirmPassword},  handleBlur, handleChange, handleSubmit, invalid,
      pristine, save, submitting, fields } = this.props;

    return (
      <Panel className="create-password-form">
        <form noValidate onSubmit={handleSubmit(onSubmit)}>
          <fieldset>
            <div className="form-group">
              <div className={'form-group-lg' + setFieldDecor(password.error, password.touched)}>
                <label className="control-label" htmlFor="password">Password</label>

                <input type="password"
                       placeholder="Password"
                       ref="password"
                       id="password"
                       className="form-control"
                  {...password}
                />
              </div>

              {password.error && password.touched && <div className="text-danger">Password is {password.error}</div>}
            </div>

            <div className="form-group">
              <div className={'form-group-lg' + setFieldDecor(confirmPassword.error, confirmPassword.touched)}>
                <label className="control-label" htmlFor="confirmPassword">Repeat Password</label>

                <input type="password"
                       placeholder="Repeat Password"
                       ref="confirmPassword"
                       id="confirmPassword"
                       className="form-control"
                  {...confirmPassword}
                />
              </div>

              {confirmPassword.error && confirmPassword.touched &&
              <div className="text-danger">Password is {confirmPassword.error}</div>}
            </div>

            { this.props.password && <p className="common-error">{this.props.password.message}</p> }

            <Button bsStyle="success"
                    bsSize="large"
                    block
                    type="submit"
                    onClick={this.showValidationErrors.bind(this)}>
              Create
            </Button>
          </fieldset>
        </form>

        <AlertBar />
      </Panel>
    )
  }
}
