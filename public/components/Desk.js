import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { Well, Glyphicon } from 'react-bootstrap';
import moment from 'moment';

export default class Desk extends Component {
  getLinkUrl(desk, deskCount) {
    if (deskCount < 10) {
      return '/';
    }

    return `/desks/${desk}/cards`;
  }

  render() {
    const { createdAtMin, desk, deskCount } = this.props;

    return (
      <div className="record-bar">
        <Link className="record-bar-link" to={this.getLinkUrl(desk, deskCount)} >
          <div className="record-bar-link-inner">
            <span className="record-bar-title">{moment(new Date(createdAtMin)).from(moment())}</span>
            <div className="record-bar-status"><span className="label label-info">{deskCount}</span></div>
          </div>

          <Glyphicon className="record-bar-icon" glyph="chevron-right" />
        </Link>
      </div>
    );
  }
}
