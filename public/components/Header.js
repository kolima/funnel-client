import React, { Component, PropTypes} from 'react';
import { Navbar, NavBrand, Nav, NavItem, MenuItem, CollapsibleNav, ButtonToolbar, Button, Input, SplitButton, NavDropdown, Col } from 'react-bootstrap';
import { Link } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as authActions from '../redux/modules/auth';
import { logout } from '../redux/modules/auth';
import _ from 'lodash';

@connect(
  state => ({
    user: state.auth.user,
    location: state.router.location
  }),
  dispatch => bindActionCreators({...authActions}, dispatch)
)
export default class Header extends Component {
  componentDidMount() {
  }

  componentWillUpdate(nextProps) {
  }

  handleLogout() {
    this.props.logout();
    this.props.history.pushState(null, '/login');
  }

  render() {
    const {user: {name = 'Profile'}} = this.props;

    return (
      <Navbar toggleNavKey={0}>
        <NavBrand>
          {<Link to={'/'}>Funnel</Link>}
        </NavBrand>

        <CollapsibleNav eventKey={0}>
          <Nav navbar>
            <LinkContainer to="/cards/create">
              <NavItem>Create a card</NavItem>
            </LinkContainer>
          </Nav>

          <Nav navbar right>
            <NavDropdown title={name} id="auth-dropdown">
              <LinkContainer to="/profile">
                <MenuItem>Profile</MenuItem>
              </LinkContainer>
              <MenuItem onClick={this.handleLogout.bind(this)}>Logout</MenuItem>
            </NavDropdown>
          </Nav>
        </CollapsibleNav>
      </Navbar>
    )
  }
}
