import React, { Component, PropTypes} from 'react';
import { Panel, Input, Button, Glyphicon } from 'react-bootstrap';
import { connect } from 'react-redux';
import { connectReduxForm, focus, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { setFieldDecor } from 'funnel-frontend-core/helpers/fieldStyle';
import cardValidation from '../../components/validations/cardValidation';
import speech from '../../helpers/speech';

import * as cardsActions from '../../redux/modules/cards';
import * as alertBarActions from 'funnel-frontend-core/modules/alert';


const MAX_CARDS_IN_DESK = 10;

let initFields = [
  'original',
  'translation'
];

@connect(
  state => ({}),
  dispatch => bindActionCreators({...cardsActions, ...alertBarActions}, dispatch)
)

@connectReduxForm({
  form: 'card',
  fields: initFields,
  validate: cardValidation
})

export default
class CardForm extends Component {
  constructor(props) {
    super(props);
  }

  componentWillUpdate(nextProps) {
    if (nextProps.loadingCreate) {
      this.alreadyReset = false;
    }


    if (nextProps.loadedCreate && !this.alreadyReset) {
      this.alreadyReset = true;
      nextProps.resetForm();

      if (parseInt(nextProps.card.deskNumberCards, 10) === MAX_CARDS_IN_DESK) {
        nextProps.history.pushState(null, `/desks/${nextProps.card.desk}/cards`);
      }
    }
  }

  runSpeech() {
    speech(this.props.fields.original.value);
  }

  showValidationErrors() {
    window.scrollTo(0, 0);
    this.props.showErrorAlert();
  }

  render() {
    const { onSubmit, fields: {original, translation},  handleBlur, handleChange, handleSubmit, invalid,
      pristine, save, submitting, fields } = this.props;

    return (
      <Panel className="card-form">
        <pre className="bg-primary">
          1. Представляем русское слово (если тяжело представить переходим к следующему слову).
          2. Кодируем иностранное слово (если тяжело представить переходим к следующему слову).
          3. Соединение 1 и 2 образ в эдинную картинку.
          4. Пишем на карточке слово. И проговариваем его 3 раза. И представляем результирующий образ. Делать это одновременно.
          Пример: Cut - резать. Первое что представляется об резать - ножницы и об cut - каток.
          Каток который с ножницами режет кур. Соединаем в общую картину. На одной стороне пишет - cut.
          Одновремено представляя общий образ и проговаривая 3 раза слово - cut, а на другой стороне перевод.
          Первое 500 - 1000 образных существительных, дальше глаголы и любые слова.
          Транскрипция: Если мы не уверены как правильно произноситься звук или не уверены, то под этой буквой пишем транскрипцию (или с наголосом), желательно красным цветом.
          Исключения: Можно дописывать артикль к слову, если он является исключением (ты думал что он должно быть, но на самом деле таким не является).
          Характеристики: Например придумываем для on, in (фразовые глаголы) или для префиксов (un, in и т.д.), которые будут характеристиками для основного слова.
        </pre>
        <form noValidate onSubmit={handleSubmit(onSubmit)}>
          <fieldset>
            <div className="form-group">
              <div className={'form-group-lg' + setFieldDecor(translation.error, translation.touched)}>
                <label className="control-label" htmlFor="translation">Translation</label>

                <input type="text"
                       placeholder="Translation"
                       ref="translation"
                       id="translation"
                       className="form-control"
                       autoComplete="off"
                  {...translation}
                />
              </div>

              {translation.error && translation.touched &&
              <div className="text-danger">Translation is {translation.error}</div>}
            </div>

            <div className="form-group">
              <div className={'form-group-lg' + setFieldDecor(original.error, original.touched)}>
                <label className="control-label" htmlFor="original">Original</label>
                <div className="input-group">
                  <input type="text"
                         placeholder="Original"
                         ref="original"
                         id="original"
                         className="form-control"
                         autoComplete="off"
                    {...original}
                  />
                  <span className="input-group-btn">
                    <button className="btn btn-default btn-lg" type="button" onClick={this.runSpeech.bind(this)}><Glyphicon glyph="volume-up"/></button>
                  </span>
                </div>
              </div>

              {original.error && original.touched && <div className="text-danger">Original is {original.error}</div>}
            </div>

            <Button bsStyle="success"
                    bsSize="large"
                    block
                    type="submit"
                    onClick={this.showValidationErrors.bind(this)}>
              Create
            </Button>
          </fieldset>
        </form>
      </Panel>
    )
  }
}
