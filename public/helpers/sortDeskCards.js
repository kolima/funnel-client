import _ from 'lodash';

export function sortDeskCards(cards) {
  let typeOfCircle;
  let countDone = {
    original: 0,
    translation: 0
  };

  cards.forEach((card) => {
    card.originalSuccess = parseInt(card.originalSuccess, 10);
    card.translationSuccess = parseInt(card.translationSuccess, 10);
    card.originalUnbeaten = parseInt(card.originalUnbeaten, 10);
    card.translationUnbeaten = parseInt(card.translationUnbeaten, 10);


    if (card.originalSuccess) {
      countDone.original += card.originalSuccess;
    }

    if (card.translationSuccess) {
      countDone.translation += card.translationSuccess;
    }
  });

  if ((countDone.translation % cards.length) || !countDone.translation) {
    typeOfCircle = 'translation';
  } else if ((countDone.original % cards.length) || !countDone.original) {
    typeOfCircle = 'original';
  } else  if (countDone.original > countDone.translation) {
    typeOfCircle = 'translation';
  } else if (countDone.original < countDone.translation) {
    typeOfCircle = 'original';
  } else {
    typeOfCircle = 'translation';
  }


  cards.sort((a, b) => {
    if (a[`${typeOfCircle}Unbeaten`] > b[`${typeOfCircle}Unbeaten`]) {
      return 1;
    } else if (a[`${typeOfCircle}Unbeaten`] < b[`${typeOfCircle}Unbeaten`]) {
      return -1;
    } else {
      return 0;
    }
  });

  return {cards, typeOfCircle};
}

export function removeMaxSuccessCard(cards, typeOfCircle) {
  let successType =`${typeOfCircle}Success`;
  let min = _.min(cards, successType)[successType];

  _.remove(cards, function(card) {
    return min !== card[successType];
  });

  return cards;
}
