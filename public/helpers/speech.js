export default function speech(text) {
  let msg = new SpeechSynthesisUtterance();
  let voices = window.speechSynthesis.getVoices();
  msg.voice = voices.filter(function(voice) { return voice.name == 'Google UK English Male'; })[0];
  msg.volume = 1; // 0 to 1
  msg.text = text;

  speechSynthesis.speak(msg);
}
