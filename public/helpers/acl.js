'use strict';

import base64 from 'funnel-frontend-core/helpers/base64';
import CookieDough from 'cookie-dough';

export default class Acl {
  static getCookies(serverCookies) {
    let result = {};

    if (serverCookies) {
      result.user = JSON.parse(serverCookies.get('funnelUser') || '{}');
    } else {
      let clientCookies = CookieDough();

      result.user = JSON.parse(clientCookies.get('funnelUser') || '{}');
    }

    return result;
  }

  static app(store, serverCookies) {
    return (nextState, replaceState) => {
      let user, state;

      try {
        state = store.getState();
        user = state.auth.user;
      } catch (err) {
        user = Acl.getCookies(serverCookies).user;
      }

      if (!user || !user.token) {
        replaceState(null, '/login');
      }
    };
  }

  static login(store, serverCookies) {
    return (nextState, replaceState) => {
      let user, state;

      try {
        state = store.getState();
        user = state.auth.user;
      } catch (err) {
        user = Acl.getCookies(serverCookies).user
      }

      if (user && user.token) {
        replaceState(null, '/');
      }
    }
  }
}
