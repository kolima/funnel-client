import React, { Component, PropTypes } from 'react';

export default class NoMatchPage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h1>Page not found</h1>
      </div>
    );
  }
}
