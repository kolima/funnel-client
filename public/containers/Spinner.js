import React, { Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as spinnerActions from '../redux/modules/spinner';

@connect(
  state => ({
    isVisible: state.spinner.isVisible,
    isDidMount: state.spinner.isDidMount
  }),
  dispatch => bindActionCreators(spinnerActions, dispatch)
)
export default
class ResponseSpinner extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.didMountSpinner();
  }

  htmlSpinner() {
    const { isVisible, isDidMount } = this.props;
    return (
      <div>
        {isVisible && isDidMount &&
        <div className="overlay">
          <span className="spinner"><span></span></span>
        </div>
        }
      </div>
    );
  }

  render() {
    const { children, isVisible } = this.props;

    return (
      <div className="wrapper">
        { children }

        { this.htmlSpinner() }
      </div>
    );
  }
}
