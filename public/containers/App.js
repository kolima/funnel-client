import React, { Component, PropTypes } from 'react';
import Header from '../components/Header'
import ErrorAlertBar from 'funnel-frontend-core/components/AlertBar/ErrorAlertBar';
import SuccessAlertBar from 'funnel-frontend-core/components/AlertBar/SuccessAlertBar';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

@connect(
  state => ({
    text: state.alert.text
  })
)
export default
class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { children } = this.props;

    return (
      <div>
        <ErrorAlertBar message={this.props.text} />
        <SuccessAlertBar message={this.props.text} />
        <Header history={this.props.history} />
        {children}
      </div>
    );
  }
}
