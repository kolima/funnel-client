import React, { Component, PropTypes } from 'react';
import PasswordForm from '../components/PasswordForm/PasswordForm';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../redux/modules/users';
import * as alertBarActions from 'funnel-frontend-core/modules/alert';
import * as authActions from '../redux/modules/auth';

@connect(
  state => ({
    createPasswordLoaded: state.auth.createPasswordLoaded
  }),
  dispatch => bindActionCreators({...userActions, ...alertBarActions, ...authActions}, dispatch)
)

export default
class CreatePasswordPage extends Component {
  constructor(props) {
    super(props);
  }

  componentWillUpdate(nextProps) {
    if (nextProps.createPasswordLoaded) {
      nextProps.history.pushState(null, '/intro');
    }
  }

  handleSubmit(data) {
    this.props.hideValidationAlert();

    let token = this.props.location.query.token;

    this.props.userCreatePassword({
      password: data.password,
      token: token
    });
  }

  render() {
    return (
      <main className="content container flex-container">
        <PasswordForm onSubmit={this.handleSubmit.bind(this)}/>
      </main>
    );
  }
}
