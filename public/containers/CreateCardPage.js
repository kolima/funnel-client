import React, { Component, PropTypes } from 'react';
import CardForm from '../components/CardForm/CardForm';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {reset} from 'redux-form';
import * as alertBarActions from 'funnel-frontend-core/modules/alert';
import * as cardsActions from '../redux/modules/cards';

@connect(
  state => ({
    loadedCreate: state.cards.loadedCreate,
    loadingCreate: state.cards.loadingCreate,
    card: state.cards.card
  }),
  dispatch => bindActionCreators({...alertBarActions, ...cardsActions}, dispatch)
)

export default
class CreateCardPage extends Component {
  componentWillUnmount() {
    this.props.clearCards();
  }

  handleSubmit(data) {
    this.props.hideValidationAlert();
    this.props.createCard({
      original: data.original.toLowerCase().trim(),
      translation: data.translation.toLowerCase().trim()
    });
  }

  render() {
    const {loadedCreate, loadingCreate, card, history} = this.props;

    return (
      <main className="content container">
        <CardForm onSubmit={this.handleSubmit.bind(this)} history={history} loadedCreate={loadedCreate} card={card} loadingCreate={loadingCreate}/>
      </main>
    );
  }
}
