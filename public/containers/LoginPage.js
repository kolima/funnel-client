import React, { Component, PropTypes } from 'react';
import { Input, ButtonToolbar, Button, Panel } from 'react-bootstrap';
import { Link } from 'react-router';
import * as authActions from '../redux/modules/auth';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {connectReduxForm, focus, initialize} from 'redux-form';
import loginValidation from '../components/validations/loginValidation';
import { setFieldDecor } from 'funnel-frontend-core/helpers/fieldStyle';
import * as alertBarActions from 'funnel-frontend-core/modules/alert';
import AlertBar from 'funnel-frontend-core/components/AlertBar/ErrorAlertBar';

let initFields = [
  'email',
  'password'
];

@connect(
  state => ({
    user: state.auth.user,
    loggingIn: state.auth.loggingIn,
    authError: state.auth.authError,
    isErrorVisible: state.auth.isErrorVisible
  }),
  dispatch => bindActionCreators({...authActions, ...alertBarActions}, dispatch)
)
@connectReduxForm({
  form: 'login',
  fields: initFields,
  validate: loginValidation
})
export default
class LoginPage extends Component {
  static propTypes = {
    user: PropTypes.object,
    login: PropTypes.func.isRequired,
    loggingIn: PropTypes.bool.isRequired,
    history: PropTypes.object.isRequired
  };

  componentWillUpdate(nextProps) {
    if (this.props.isErrorVisible) {
      this.props.cleanAuthErrors();
    }

    if (nextProps.user && nextProps.user.token) {
      nextProps.history.pushState(null, '/');
    }
  }

  handleSubmit(data) {
    this.props.hideValidationAlert();

    this.props.login({
      email: data.email,
      password: data.password
    });
  }

  showValidationErrors() {
    window.scrollTo(0, 0);
    this.props.showErrorAlert();
  }

  componentWillUnmount() {
    this.props.cleanAuthErrors();
    this.props.resetForm();
  }

  render() {
    const { loggingIn, fields: {email, password},  handleBlur, handleChange, handleSubmit, invalid,
      pristine, save, submitting, fields } = this.props;

    return (
      <main className="container flex-container">
        <div className="authorization-form">
          <form noValidate onSubmit={handleSubmit(this.handleSubmit.bind(this))}>

            <div className="form-group">
              <div className={'form-group-lg' +  setFieldDecor(email.error, email.touched)}>
                <input type="email"
                       id="email"
                       className="form-control"
                       ref="email"
                       placeholder="Login"
                  {...email}
                />
              </div>

              {email.error && email.touched && <div className="text-danger">Login is {email.error}</div>}
            </div>

            <div className="form-group">
              <div className={'form-group-lg' +  setFieldDecor(password.error, password.touched)}>
                <input type="password"
                       id="password"
                       className="form-control"
                       ref="password"
                       placeholder="Password"
                  {...password}
                />
              </div>

              {password.error && password.touched && <div className="text-danger">Password is {password.error}</div>}
            </div>

            { this.props.authError && <p className="common-error">{this.props.authError.message}</p> }

            <ButtonToolbar>
              <Button bsStyle="success"
                      className="btn-submit"
                      bsSize="large"
                      type="submit"
                      onClick={this.showValidationErrors.bind(this)}
                      disabled={loggingIn}>
                Login
              </Button>

              <Link className="btn btn-link" to={'/password/forgot'}>Forgot Password</Link>
            </ButtonToolbar>
          </form>
        </div>

        <AlertBar />
      </main>
    );
  }
}
