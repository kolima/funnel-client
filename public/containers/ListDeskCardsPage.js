import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import { ButtonToolbar, Button, Row, Table, Glyphicon, ButtonGroup, Well } from 'react-bootstrap';
import * as alertBarActions from 'funnel-frontend-core/modules/alert';
import * as cardsActions from '../redux/modules/cards';
import speech from '../helpers/speech';

@connect(
  state => ({
    cards: state.cards.cards,
    isOpenCard: state.cards.isOpenCard,
    typeOfCircle: state.cards.typeOfCircle,
    lastClickedButton: state.cards.lastClickedButton
  }),
  dispatch => bindActionCreators({...alertBarActions, ...cardsActions}, dispatch)
)

export default
class CreatePasswordPage extends Component {
  static propTypes = {
    cards: PropTypes.array
  };

  componentWillMount() {
    this.props.showDeskCards(this.props.routeParams.desk);
  }

  componentWillUnmount() {
    this.props.clearCards();
  }

  componentWillUpdate(nextProps) {
    if (nextProps.typeOfCircle === 'translation' && nextProps.isOpenCard) {
      speech(nextProps.cards[0].original);
    }
  }

  render() {
    const {cards = [], clickUp, clickDown, isOpenCard, typeOfCircle, lastClickedButton, routeParams: {desk}} = this.props;
    const [card = {}] = cards;

    return (
      <div className="container">
        <pre className="bg-primary">
          1-ое повторение - с русского на иностранный
          2-ое - с иностарнного на русский
          Проходов минимум 3.
          Если все верно карточка в конец.
          Звучание ошибочно - 3 раза проговариваем слово представляя результирующий образ. Кледем не далеко, через несколько карточек.
          Не вспоминается - начинаем все с начала, как будто изучаем как новое.
          Не нужно вспоминать дольше 3 сек., значит связь не прочна и проще перезаписать её.
          7-10 карточек в одной пачке. после 3-ех пачек повторить все вместе. (1 повторение (рус-англ. и англ.-рус.). Если повторяются ошибки повторить два раза.
        </pre>

          <Well className="col-xs-12 col-sm-5 col-md-5 col-lg-5 text-center">
            Original:
            <h3 className="">{((typeOfCircle === 'original') || isOpenCard) && card.original}</h3>
          </Well>
          <Well className="col-xs-12 col-sm-1 col-md-1 col-lg-1 text-center">
            {(isOpenCard && (lastClickedButton === 'down')) ||
              <Glyphicon glyph="glyphicon glyphicon-thumbs-up fa-5x text-success" onClick={clickUp.bind(this, {...card, desk, typeOfCircle, isOpenCard})}/>
            }
          </Well>
          <Well className="col-xs-12 col-sm-1 col-md-1 col-lg-1 text-center" >
            <Glyphicon glyph="glyphicon glyphicon-thumbs-down text-danger" onClick={clickDown.bind(this, {...card, desk, typeOfCircle, isOpenCard})}/>
          </Well>
          <Well className="col-xs-12 col-sm-5 col-md-5 col-lg-5 text-center">
            Translation:
            <h3>{((typeOfCircle === 'translation') || isOpenCard) && card.translation}</h3>
          </Well>
      </div>
    );
  }
}
