import React, { Component, PropTypes } from 'react';
import PasswordForm from '../components/PasswordForm/PasswordForm';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../redux/modules/users';
import * as alertBarActions from 'funnel-frontend-core/modules/alert';
import * as authActions from '../redux/modules/auth';

@connect(
  state => ({
    isPasswordSuccessfulyEdited: state.users.isPasswordSuccessfulyEdited
  }),
  dispatch => bindActionCreators({...userActions, ...alertBarActions, ...authActions}, dispatch)
)

export default
class EditPasswordPage extends Component {
  constructor(props) {
    super(props);
  }

  handleSubmit(data) {
    this.props.hideValidationAlert();

    let token = this.props.location.query.token;

    this.props.userEditPassword({
      password: data.password,
      token: token
    });

    this.props.history.pushState(null, '/');
  }

  render() {

    return (
      <main className="content container flex-container">
        <PasswordForm onSubmit={this.handleSubmit.bind(this)}/>
      </main>
    );
  }
}
