import React, { Component, PropTypes } from 'react';
import { Input, ButtonToolbar, Button } from 'react-bootstrap';
import { Link } from 'react-router';
import {connect} from 'react-redux';
import {connectReduxForm, focus, initialize} from 'redux-form';
import forgotPasswordValidation from '../components/validations/forgotPasswordValidation';
import { setFieldDecor } from 'funnel-frontend-core/helpers/fieldStyle';
import {bindActionCreators} from 'redux';
import AlertBar from 'funnel-frontend-core/components/AlertBar/ErrorAlertBar';
import * as alertBarActions from 'funnel-frontend-core/modules/alert';
import * as authActions from '../redux/modules/auth';

let initFields = [
  'email'
];

@connectReduxForm({
  form: 'forgot-password',
  fields: initFields,
  validate: forgotPasswordValidation
})

@connect(
  state => ({
    authError: state.auth.authError,
    isErrorVisible: state.auth.isErrorVisible,
    isMessageVisible: state.auth.isMessageVisible,
    isForgotPasswordSuccess: state.auth.isForgotPasswordSuccess
  }),
  dispatch => bindActionCreators({...authActions, ...alertBarActions}, dispatch)
)

export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this._isFormReset = false;
  }

  handleSubmit(data) {
    this.props.hideValidationAlert();

    this.props.forgotPassword({
      email: data.email
    });
  }

  componentWillUpdate(nextProps) {
    if (this.props.isErrorVisible) {
      this.props.cleanAuthErrors();
      this.props.resetForm();
    }

    if (nextProps.isMessageVisible && !this._isFormReset) {
      nextProps.resetForm();
      this._isFormReset = true;
    }
  }

  showValidationErrors() {
    window.scrollTo(0, 0);
    this.props.showErrorAlert();
  }

  componentWillUnmount() {
    this.props.cleanAuthErrors();
    this.props.resetForm();
  }

  render() {
    const { fields: {email},  handleBlur, handleChange, handleSubmit, invalid, pristine, save, submitting, fields } = this.props;

    return (
      <main className="container flex-container">
        <div className="authorization-form">
          <form noValidate onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
            <fieldset>
              <div className="form-group">
                <div className={'form-group-lg' + setFieldDecor(email.error, email.touched)}>
                  <label className="control-label" htmlFor="email">Email:</label>

                  <input type="email"
                         id="email"
                         className="form-control"
                         placeholder="Email"
                         disabled={this.props.isMessageVisible}
                    {...email}
                  />
                </div>

                {email.error && email.touched && <div className="text-danger">E-Mail is {email.error}</div>}
              </div>

              { this.props.authError && <p className="common-error">{this.props.authError.message}</p> }

              { this.props.isMessageVisible && <p className="text-success text-center">Password reset link was generated and sent to your email</p> }

              <ButtonToolbar>
                <Button bsStyle="success"
                        className="btn-submit"
                        bsSize="large"
                        type="submit"
                        onClick={this.showValidationErrors.bind(this)}
                        disabled={this.props.isMessageVisible}
                >
                  Send
                </Button>

                <Link className="btn btn-link" to={'/login'}>Login</Link>
              </ButtonToolbar>
            </fieldset>
          </form>
        </div>

        <AlertBar />
      </main>
    );
  }
}
