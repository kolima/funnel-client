import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { Link } from 'react-router';
import { ButtonToolbar, Button, Row, Table, Glyphicon, ButtonGroup, Well } from 'react-bootstrap';
import Desk from '../components/Desk';
import * as cardsActions from '../redux/modules/cards';

@connect(
  state => ({
    desks: state.cards.desks
  }),
  dispatch => bindActionCreators(cardsActions, dispatch)
)
export default
class DashboardPage extends Component {
  static propTypes = {
    desks: PropTypes.array
  };

  componentWillMount() {
    this.props.showDesks();
  }

  render() {
    const { desks } = this.props;

    return (
      <div className="container">
        <Row>
          <Well className="col-xs-12">
            <Link className="btn-block btn btn-lg btn-primary" to={'/cards/create'} block>Create a Card</Link>
          </Well>
        </Row>
        <Row>
          {desks.map((desk, index) =>
            <Desk {...desk} key={index} />
          )}
        </Row>
      </div>
    );
  }
}
