import 'babel-core/polyfill';
import 'Base64';
import 'funnel-frontend-core/sass/_base.scss';
import 'react-widgets/dist/css/react-widgets.css';

import React from 'react';
import { render } from 'react-dom';
import createStore from './redux/create';
import { Provider } from 'react-redux';
import { reduxReactRouter, ReduxRouter } from 'redux-router';

import getRoutes from './routes';
import Api from 'funnel-frontend-core/helpers/api';
import createHistory from 'history/lib/createBrowserHistory';

const client = new Api();
const store = createStore(reduxReactRouter, getRoutes, createHistory, client, window.__data);
const dest = document.getElementById('root');

const component = (
  <ReduxRouter routes={getRoutes(undefined, store)} />
);

render(
  <Provider store={store} key="provider">
    {component}
  </Provider>,
  dest
);
