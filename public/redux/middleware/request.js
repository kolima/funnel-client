import * as spinnerActions from '../modules/spinner';

export default function request(client) {
  let authToken;

  const typeShowSpinner =  spinnerActions.showSpinner();
  const typeHideSpinner = spinnerActions.hideSpinner();

  return ({dispatch, getState}) => {
    return next => action => {
      if (typeof action === 'function') {
        return action(dispatch, getState);
      }

      const { promise, types, ...rest } = action;
      if (!promise) {
        return next(action);
      }

      try {
        authToken = getState().auth.user.token;
      } catch (err) {}

      client.headers = {
        auth: authToken
      };

      const [REQUEST, SUCCESS, FAILURE] = types;

      next({...rest, ...typeShowSpinner});
      next({...rest, type: REQUEST});
      return promise(client).then(
        (result) => {
          next({...rest, ...typeHideSpinner});
          return next({...rest, result, type: SUCCESS});
        },
        (error) => {
          next({...rest, ...typeHideSpinner});
          return next({...rest, error, type: FAILURE});
        }
      );
    };
  };
}
