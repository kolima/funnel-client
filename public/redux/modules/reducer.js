import { combineReducers } from 'redux';
import { routerStateReducer as router } from 'redux-router';

import {reducer as form} from 'redux-form';
import alert from 'funnel-frontend-core/modules/alert';
import auth from './auth';
import users from './users';
import cards from './cards';
import spinner from './spinner';


export default function (serverCookies) {
  return combineReducers({
    router,
    form,
    spinner,
    alert,
    cards,
    auth: auth(serverCookies),
    users
  });
};
