'use strict';

import base64 from 'funnel-frontend-core/helpers/base64';
import CookieDough from 'cookie-dough';

const LOGIN = 'LOGIN';
const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const LOGIN_FAIL = 'LOGIN_FAIL';

const LOGOUT = 'LOGOUT';

const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS';
const FORGOT_PASSWORD_FAIL = 'FORGOT_PASSWORD_FAIL';

const ADMIN_CREATE_PASSWORD = 'ADMIN_CREATE_PASSWORD';
const ADMIN_CREATE_PASSWORD_SUCCESS = 'ADMIN_CREATE_PASSWORD_SUCCESS';
const ADMIN_CREATE_PASSWORD_FAIL = 'ADMIN_CREATE_PASSWORD_FAIL';

const ADMIN_EDIT_PASSWORD = 'ADMIN_EDIT_PASSWORD';
const ADMIN_EDIT_PASSWORD_SUCCESS = 'ADMIN_EDIT_PASSWORD_SUCCESS';
const ADMIN_EDIT_PASSWORD_FAIL = 'ADMIN_EDIT_PASSWORD_FAIL';

const CLEAN_AUTH_MESSAGES = 'CLEAN_AUTH_MESSAGES';
const CLEAN_ADMIN_PASSWORD_ERRORS = 'CLEAN_ADMIN_PASSWORD_ERRORS';

const SET_USER_FROM_LOCAL_STORAGE = 'SET_USER_FROM_LOCAL_STORAGE';

const PROFILE_LOAD = 'PROFILE_LOAD';
const PROFILE_LOAD_SUCCESS = 'PROFILE_LOAD_SUCCESS';
const PROFILE_LOAD_FAILED = 'PROFILE_LOAD_FAILED';

const PROFILE_UPDATE = 'PROFILE_UPDATE';
const PROFILE_UPDATE_SUCCESS = 'PROFILE_UPDATE_SUCCESS';
const PROFILE_UPDATE_FAILED = 'PROFILE_UPDATE_FAILED';

const CLEAN_PROFILE_UPDATE_STATUS = 'CLEAN_PROFILE_UPDATE_STATUS';

const COMPANY_SET = 'COMPANY_SET';

const initialState = {
  loaded: false,
  loggingIn: false,
  authError: {},
  loggingOut: false,
  createPasswordLoaded: false,
  isPasswordSuccessfulyEdited: false,
  password: {},
  profileLoadingError: {},
  profileUpdatingError: {}
};

export default function reducer(serverCookies) {
  let clientCookies = {};

  if (serverCookies) {
    initialState.user = JSON.parse(serverCookies.get('funnelUser') || '{}');
  } else {
    clientCookies = CookieDough();
    initialState.user = JSON.parse(clientCookies.get('funnelUser') || '{}');
  }

  if (initialState.user.token) {
    initialState.user = {...base64.decodeToken(initialState.user.token), ...initialState.user};
  }

  return function(state = initialState, action = {}) {
    let profile;

    switch (action.type) {
      // LOGIN
      case LOGIN:
        return {
          ...state,
          loggingIn: true
        };
      case LOGIN_SUCCESS:
        clientCookies.set('funnelUser', JSON.stringify(action.result));

        profile = base64.decodeToken(action.result.token);

        return {
          ...state,
          loggingIn: false,
          user: {...profile, token: action.result.token}
        };
      case LOGIN_FAIL:
        return {
          ...state,
          loggingIn: false,
          isErrorVisible: true,
          user: null,
          authError: action.error
        };

      // LOGOUT
      case LOGOUT:
        clientCookies.remove('funnelUser');

        return {
          ...state,
          user: {},
          loggingOut: true
        };

      // FORGOT PASSWORD
      case FORGOT_PASSWORD:
        return {
          ...state,
          isForgotPasswordLoading: true
        };
      case FORGOT_PASSWORD_SUCCESS:
        return {
          ...state,
          isMessageVisible: true,
          isForgotPasswordSuccess: true,
          isForgotPasswordLoading: false
        };
      case FORGOT_PASSWORD_FAIL:
        return {
          ...state,
          isErrorVisible: true,
          isForgotPasswordSuccess: false,
          authError: action.error
        };

      case ADMIN_CREATE_PASSWORD:
        return {
          ...state,
          loading: true
        };
      case ADMIN_CREATE_PASSWORD_SUCCESS:
        clientCookies.set('funnelUser', JSON.stringify(action.result));

        profile = base64.decodeToken(action.result.token);

        return {
          ...state,
          loading: false,
          createPasswordLoaded: true,
          user: {...profile, token: action.result.token}
        };
      case ADMIN_CREATE_PASSWORD_FAIL:
        return {
          ...state,
          loading: false,
          createPasswordLoaded: false,
          isErrorVisible: true,
          password: action.error
        };

      case ADMIN_EDIT_PASSWORD:
        return {
          ...state,
          isPasswordEdited: true
        };
      case ADMIN_EDIT_PASSWORD_SUCCESS:
        clientCookies.set('funnelUser', JSON.stringify(action.result));

        profile = base64.decodeToken(action.result.token);

        return {
          ...state,
          isPasswordEdited: false,
          isPasswordSuccessfulyEdited: true,
          user: {...profile, token: action.result.token}
        };
      case ADMIN_EDIT_PASSWORD_FAIL:
        return {
          ...state,
          isPasswordEdited: false,
          isErrorVisible: true,
          isPasswordSuccessfulyEdited: false,
          password: action.error
        };

      case CLEAN_ADMIN_PASSWORD_ERRORS:
        return {
          ...state,
          isErrorVisible: false,
          password: {}
        };

      // CLEAN AUTH MESSAGES
      case CLEAN_AUTH_MESSAGES:
        return {
          ...state,
          isErrorVisible: false,
          isMessageVisible: false,
          authError: {}
        };

      case SET_USER_FROM_LOCAL_STORAGE:
        return {
          ...state,
          isErrorVisible: false,
          isMessageVisible: false,
          user: action.data.user
        };

      case PROFILE_LOAD:
        return {
          ...state,
          isProfileLoading: true
        };
      case PROFILE_LOAD_SUCCESS:
        return {
          ...state,
          user: {...state.user, ...action.result},
          isProfileLoading: false,
          isProfileLoaded: true
        };
      case PROFILE_LOAD_FAILED:
        return {
          ...state,
          isProfileLoading: false,
          isProfileLoaded: false,
          profileLoadingError: action.error
        };

      case PROFILE_UPDATE:
        return {
          ...state,
          isProfileUpdating: true
        };
      case PROFILE_UPDATE_SUCCESS:
        clientCookies.set('funnelUser', JSON.stringify(action.result), clientCookies);

        profile = base64.decodeToken(action.result.token);

        return {
          ...state,
          isProfileUpdating: false,
          isProfileUpdated: true,
          user: {...profile, token: action.result.token}
        };
      case PROFILE_UPDATE_FAILED:
        return {
          ...state,
          isProfileUpdating: false,
          isProfileUpdated: false,
          profileUpdatingError: action.error
        };

      case CLEAN_PROFILE_UPDATE_STATUS:
        return {
          ...state,
          isProfileUpdated: false
        };

      default:
        return state;
    }
  }
}

export function login({email, password}) {
  return {
    types: [LOGIN, LOGIN_SUCCESS, LOGIN_FAIL],
    promise: (client) => client.post(`${API_URL}/auth/signin`, {
      data: {
        email, password
      }
    })
  };
}

export function logout() {
  return {
    type: LOGOUT
  };
}

export function cleanAuthErrors() {
  return {
    type: CLEAN_AUTH_MESSAGES
  }
}

export function forgotPassword({email}) {
  return {
    types: [FORGOT_PASSWORD, FORGOT_PASSWORD_SUCCESS, FORGOT_PASSWORD_FAIL],
    promise: (client) => client.post(`${API_URL}/auth/password/forgot`, {
      data: {
        email
      }
    })
  }
}

export function adminCreatePassword({password, token}) {
  return {
    types: [ADMIN_CREATE_PASSWORD, ADMIN_CREATE_PASSWORD_SUCCESS, ADMIN_CREATE_PASSWORD_FAIL],
    promise: (client) => client.post(`${API_URL}/auth/password`, {
      data: {
        password, token
      }
    })
  };
}

export function adminEditPassword({password, token}) {
  return {
    types: [ADMIN_EDIT_PASSWORD, ADMIN_EDIT_PASSWORD_SUCCESS, ADMIN_EDIT_PASSWORD_FAIL],
    promise: (client) => client.put(`${API_URL}/auth/password`, {
      data: {
        password, token
      }
    })
  };
}

export function cleanPasswordErrors() {
  return {
    type: CLEAN_ADMIN_PASSWORD_ERRORS
  }
}

export function setUserFromLocalStorage(user) {
  return {
    type: SET_USER_FROM_LOCAL_STORAGE,
    data: {
      user
    }
  };
}

export function showProfile() {
  return {
    types: [PROFILE_LOAD, PROFILE_LOAD_SUCCESS, PROFILE_LOAD_FAILED],
    promise: (client) => client.get(`${API_URL}/admins/me`)
  };
}

export function updateProfile({name, password}) {
  return {
    types: [PROFILE_UPDATE, PROFILE_UPDATE_SUCCESS, PROFILE_UPDATE_FAILED],
    promise: (client) => client.put(`${API_URL}/admins/me`, {
      data: {
        name, password
      }
    })
  };
}

export function cleanProfileUpdateStatus(){
  return {
    type: CLEAN_PROFILE_UPDATE_STATUS
  }
}
