'use strict';

import humps from 'humps';
import {sortDeskCards, removeMaxSuccessCard} from '../../helpers/sortDeskCards';

const CARD_CREATE = 'CARD_CREATE';
const CARD_CREATE_SUCCESS = 'CARD_CREATE_SUCCESS';
const CARD_CREATE_FAILED = 'CARD_CREATE_FAILED';

const DESKS_LIST = 'DESKS_LIST';
const DESKS_LIST_SUCCESS = 'DESKS_LIST_SUCCESS';
const DESKS_LIST_FAILED = 'DESKS_LIST_FAILED';

const DESK_CARDS_LIST = 'DESK_CARDS_LIST';
const DESK_CARDS_LIST_SUCCESS = 'DESK_CARDS_LIST_SUCCESS';
const DESK_CARDS_LIST_FAILED = 'DESK_CARDS_LIST_FAILED';


const CARD_REPEAT_UP = 'CARD_REPEAT_UP';
const CARD_REPEAT_UP_SUCCESS = 'CARD_REPEAT_UP_SUCCESS';
const CARD_REPEAT_UP_FAILED = 'CARD_REPEAT_UP_FAILED';

const CARD_REPEAT_DOWN = 'CARD_REPEAT_DOWN';
const CARD_REPEAT_DOWN_SUCCESS = 'CARD_REPEAT_DOWN_SUCCESS';
const CARD_REPEAT_DOWN_FAILED = 'CARD_REPEAT_DOWN_FAILED';

const CARD_OPEN_TOGGLE = 'CARD_OPEN_TOGGLE';

const CARDS_CLEAR = 'CARDS_CLEAR';

const initialState = {
  desks: [],
  cards: [],
  typeOfCircle: 'translation',
  isOpenCard: false,
  card: {}
};

export default function reducer(state = initialState, action = {}) {
  let copiedCards, sorted, serverCards;

  switch (action.type) {
    case DESKS_LIST:
      return {
        ...state,
        loadingList: true
      };
    case DESKS_LIST_SUCCESS:
      return {
        desks: humps.camelizeKeys(action.result),
        loadingList: false,
        loadedList: true
      };

    case DESKS_LIST_FAILED:
      return {
        ...state,
        loadingList: false,
        loadedList: false,
        error: action.error
      };

    case DESK_CARDS_LIST:
      return {
        ...state,
        loadingDesk: true
      };
    case DESK_CARDS_LIST_SUCCESS:
      let {cards, typeOfCircle} = sortDeskCards(humps.camelizeKeys(action.result));
      serverCards = [...cards];

      cards = removeMaxSuccessCard(cards, typeOfCircle);

      return {
        serverCards,
        cards,
        typeOfCircle,
        loadingDesk: false,
        loadedDesk: true
      };
    case DESK_CARDS_LIST_FAILED:
      return {
        ...state,
        loadingDesk: false,
        loadedDesk: false,
        error: action.error
      };

    case CARD_CREATE:
      return {
        ...state,
        loadingCreate: true,
        loadedCreate: false
      };
    case CARD_CREATE_SUCCESS:
      return {
        card: humps.camelizeKeys(action.result),
        loadingCreate: false,
        loadedCreate: true
      };
    case CARD_CREATE_FAILED:
      return {
        ...state,
        loadingCreate: false,
        loadedCreate: false,
        error: action.error
      };

    case CARD_REPEAT_UP:
      return {
        ...state,
        loadingUp: true
      };
    case CARD_REPEAT_UP_SUCCESS:
      let serverUpRepeat = humps.camelizeKeys(action.result);

      let viewCards = [];
      let indexRemoveCard = 0;

      copiedCards = state.serverCards.map((card) => {
        let copiedCard = {...card};

        if (serverUpRepeat.cardId === copiedCard.id) {
          copiedCard[`${state.typeOfCircle}Success`] += 1;
          copiedCard[`${state.typeOfCircle}Unbeaten`] += 1;
        }

        return copiedCard;
      });

      sorted = sortDeskCards(copiedCards);

      if (state.typeOfCircle === sorted.typeOfCircle) {
        viewCards = state.cards.map((card, index) => {
          let copiedCard = {...card};

          if (serverUpRepeat.cardId === copiedCard.id) {
            indexRemoveCard = index;
          }

          return copiedCard;
        });

        viewCards.splice(indexRemoveCard, 1);
      } else {
        viewCards = [...sorted.cards];
      }

      //todo: empty cards if distance is big
      viewCards = removeMaxSuccessCard(viewCards, sorted.typeOfCircle);

      return {
        ...state,
        serverCards: sorted.cards,
        cards: viewCards,
        typeOfCircle: sorted.typeOfCircle,
        isOpenCard: !state.isOpenCard,
        lastClickedButton: 'up',
        loadingUp: false,
        loadedUp: true
      };
    case CARD_REPEAT_UP_FAILED:
      return {
        ...state,
        loadingUp: false,
        loadedUp: false,
        error: action.error
      };

    case CARD_REPEAT_DOWN:
      return {
        ...state,
        loadingDown: true
      };
    case CARD_REPEAT_DOWN_SUCCESS:
      let serverDownRepeat = humps.camelizeKeys(action.result);
      let indexFailCard = 0;
      let percentDeskWhereInsertCard = 25;
      let failCard;
      let indexWhereInsertFailCard = Math.round((state.cards.length / 100) * percentDeskWhereInsertCard);


      serverCards = state.serverCards.map((card, index) => {
        let copiedCard = {...card};

        if (serverDownRepeat.cardId === copiedCard.id) {
          copiedCard[`${state.typeOfCircle}Unbeaten`] = 0;
        }

        return copiedCard;
      });


      copiedCards = state.cards.map((card, index) => {
        let copiedCard = {...card};

        if (serverDownRepeat.cardId === copiedCard.id) {
          indexFailCard = index;
        }

        return copiedCard;
      });

      failCard = copiedCards.splice(indexFailCard, 1);
      copiedCards.splice(indexWhereInsertFailCard, 0, failCard[0]);

      return {
        ...state,
        serverCards,
        cards: copiedCards,
        isOpenCard: !state.isOpenCard,
        lastClickedButton: 'down',
        loadingDown: false,
        loadedDown: true
      };
    case CARD_REPEAT_DOWN_FAILED:
      return {
        ...state,
        loadingDown: false,
        loadedDown: false,
        error: action.error
      };

    case CARD_OPEN_TOGGLE:
      return {
        ...state,
        lastClickedButton: action.data,
        isOpenCard: !state.isOpenCard
      };

    case CARDS_CLEAR:
      return {
        ...initialState
      };

    default:
      return state;
  }
};


export function showDesks() {
  return {
    types: [DESKS_LIST, DESKS_LIST_SUCCESS, DESKS_LIST_FAILED],
    promise: (client) => client.get(`${API_URL}/desks`)
  };
}

export function showDeskCards(desk) {
  return {
    types: [DESK_CARDS_LIST, DESK_CARDS_LIST_SUCCESS, DESK_CARDS_LIST_FAILED],
    promise: (client) => client.get(`${API_URL}/desks/${desk}/cards`)
  };
}

export function createCard({original, translation}) {
  return {
    types: [CARD_CREATE, CARD_CREATE_SUCCESS, CARD_CREATE_FAILED],
    promise: (client) => client.post(`${API_URL}/cards`, {
      data: {original, translation}
    })
  };
}

export function clickUp({id, originalWordId, translationWordId, typeOfCircle, isOpenCard, desk}) {
  if (!isOpenCard) {
    return {
      type: CARD_OPEN_TOGGLE,
      data: 'up'
    }
  }

  let wordId = translationWordId;

  if (typeOfCircle === 'original') {
    wordId = originalWordId;
  }

  return {
    types: [CARD_REPEAT_UP, CARD_REPEAT_UP_SUCCESS, CARD_REPEAT_UP_FAILED],
    promise: (client) => client.post(`${API_URL}/desks/${desk}/cards/${id}/words/${wordId}/repeats/success`)
  };
}

export function clickDown({id, originalWordId, translationWordId, typeOfCircle, isOpenCard, desk}) {
  if (!isOpenCard) {
    return {
      type: CARD_OPEN_TOGGLE,
      data: 'down'
    }
  }

  let wordId = translationWordId;

  if (typeOfCircle === 'original') {
    wordId = originalWordId;
  }

  return {
    types: [CARD_REPEAT_DOWN, CARD_REPEAT_DOWN_SUCCESS, CARD_REPEAT_DOWN_FAILED],
    promise: (client) => client.post(`${API_URL}/desks/${desk}/cards/${id}/words/${wordId}/repeats/fail`)
  };
}

export function clearCards() {
  return {
    type: CARDS_CLEAR
  };
}
