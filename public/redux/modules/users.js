'use strict';

const
  USERS_LOAD = 'USERS_LOAD',
  USERS_LOAD_SUCCESS = 'USERS_LOAD_SUCCESS',
  USERS_LOAD_FAILED = 'USERS_LOAD_FAILED',

  SUPER_USER_CREATE = 'SUPER_USER_CREATE',
  SUPER_USER_CREATE_SUCCESS = 'SUPER_USER_CREATE_SUCCESS',
  SUPER_USER_CREATE_FAIL = 'SUPER_USER_CREATE_FAIL',

  USER_CREATE = 'USER_CREATE',
  USER_CREATE_SUCCESS = 'USER_CREATE_SUCCESS',
  USER_CREATE_FAIL = 'USER_CREATE_FAIL',

  SINGLE_USER_LOAD = 'SINGLE_USER_LOAD',
  SINGLE_USER_LOAD_SUCCESS = 'SINGLE_USER_LOAD_SUCCESS',
  SINGLE_USER_LOAD_FAILED = 'SINGLE_USER_LOAD_FAILED',

  SINGLE_USER_UPDATE = 'SINGLE_USER_UPDATE',
  SINGLE_USER_UPDATE_SUCCESS = 'SINGLE_USER_UPDATE_SUCCESS',
  SINGLE_USER_UPDATE_FAILED = 'SINGLE_USER_UPDATE_FAILED',

  USER_TOGGLE_SETTINGS_BLOCK = 'USER_TOGGLE_SETTINGS_BLOCK';

const USER_COMPANIES = 'USER_COMPANIES';
const USER_COMPANIES_SUCCESS = 'USER_COMPANIES_SUCCESS';
const USER_COMPANIES_FAILED = 'USER_COMPANIES_FAILED';

const USER_ADD_TO_COMPANY = 'USER_ADD_TO_COMPANY';
const USER_ADD_TO_COMPANY_SUCCESS = 'USER_ADD_TO_COMPANY_SUCCESS';
const USER_ADD_TO_COMPANY_FAILED = 'USER_ADD_TO_COMPANY_FAILED';

const USER_REMOVE_FROM_COMPANY = 'USER_REMOVE_FROM_COMPANY';
const USER_REMOVE_FROM_COMPANY_SUCCESS = 'USER_REMOVE_FROM_COMPANY_SUCCESS';
const USER_REMOVE_FROM_COMPANY_FAILED = 'USER_REMOVE_FROM_COMPANY_FAILED';

const USER_ENABLE = 'USER_ENABLE';
const USER_ENABLE_SUCCESS = 'USER_ENABLE_SUCCESS';
const USER_ENABLE_FAIL = 'USER_ENABLE_FAIL';

const USER_DISABLE = 'USER_DISABLE';
const USER_DISABLE_SUCCESS = 'USER_DISABLE_SUCCESS';
const USER_DISABLE_FAIL = 'USER_DISABLE_FAIL';

const USER_CREATE_CLEAN_ERROR = 'USER_CREATE_CLEAN_ERROR';

const initialState = {
  createUserLoaded: false,
  users: [],
  userCompanies: [],
  user: {},
  password: {},
  isSettingsVisible: false,
  isSingleUserUpdated: false,
  errorUserCreate: {},
  isUserCreateMessageVisible: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case USERS_LOAD:
      return {
        ...state,
        loading: true
      };
    case USERS_LOAD_SUCCESS:
      return {
        users: action.result,
        loading: false,
        loaded: true
      };
    case USERS_LOAD_FAILED:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };

    case SUPER_USER_CREATE:
      return {
        ...state,
        loading: true
      };
    case SUPER_USER_CREATE_SUCCESS:
      return {
        ...state,
        loading: false,
        superUserCreateSuccess: true
      };
    case SUPER_USER_CREATE_FAIL:
      return {
        ...state,
        loading: false,
        superUserCreateFail: true,
        isUserCreateMessageVisible: true,
        errorUserCreate: action.error
      };

    case USER_CREATE:
      return {
        ...state,
        userCreating: true
      };
    case USER_CREATE_SUCCESS:
      return {
        ...state,
        userCreating: false,
        userCreated: true
      };
    case USER_CREATE_FAIL:
      return {
        ...state,
        userCreating: false,
        userCreated: false,
        isUserCreateMessageVisible: true,
        errorUserCreate: action.error
      };
    case USER_CREATE_CLEAN_ERROR:
      return {
        ...state,
        isUserCreateMessageVisible: false,
        errorUserCreate: {}
      };

    case SINGLE_USER_LOAD:
      return {
        ...state,
        isSingleUserLoading: true
      };
    case SINGLE_USER_LOAD_SUCCESS:
      return {
        ...state,
        user: action.result,
        isSingleUserLoading: false,
        isSingleUserLoaded: true
      };
    case SINGLE_USER_LOAD_FAILED:
      return {
        ...state,
        isSingleUserLoading: false,
        isSingleUserLoaded: false,
        singleUserError: action.error
      };

    case SINGLE_USER_UPDATE:
      return {
        ...state,
        isSingleUserUpdating: true
      };
    case SINGLE_USER_UPDATE_SUCCESS:
      return {
        ...state,
        isSingleUserUpdating: false,
        isSingleUserUpdated: true
      };
    case SINGLE_USER_UPDATE_FAILED:
      return {
        ...state,
        isSingleUserUpdating: false,
        isSingleUserUpdated: false,
        singleUserUpdateError: action.error
      };

    case USER_TOGGLE_SETTINGS_BLOCK:
      return {
        ...state,
        isSettingsVisible: !state.isSettingsVisible
      };

    case USER_COMPANIES:
      return {
        ...state,
        userCompaniesLoading: true
      };

    case USER_COMPANIES_SUCCESS:
      return {
        ...state,
        userCompanies: action.result,
        userCompaniesLoading: false,
        userCompaniesLoaded: true
      };

    case USER_COMPANIES_FAILED:
      return {
        ...state,
        userCompaniesLoading: false,
        userCompaniesLoaded: false,
        error: action.error
      };

    case USER_ADD_TO_COMPANY:
      return {
        ...state,
        userAddToCompanyLoading: true
      };

    case USER_ADD_TO_COMPANY_SUCCESS:
      return {
        ...state,
        userCompanies: [...state.userCompanies, action.result.company],
        userAddToCompanyLoading: false,
        userAddToCompanyLoaded: true
      };

    case USER_ADD_TO_COMPANY_FAILED:
      return {
        ...state,
        userAddToCompanyLoading: false,
        userAddToCompanyLoaded: false,
        error: action.error
      };

    case USER_REMOVE_FROM_COMPANY:
      return {
        ...state,
        userRemoveFromCompanyLoading: true
      };

    case USER_REMOVE_FROM_COMPANY_SUCCESS:
      let index;

      state.userCompanies.forEach((company, companyIndex) => {
        if (action.result.company.id === company.id) {
          index = companyIndex;
        }
      });

      if (typeof index !== 'number') {
        return null;
      }

      return {
        ...state,
        userCompanies: [
          ...state.userCompanies.slice(0, index),
          ...state.userCompanies.slice(index + 1)
        ],
        userRemoveFromCompanyLoading: false,
        userRemoveFromCompanyLoaded: true
      };

    case USER_REMOVE_FROM_COMPANY_FAILED:
      return {
        ...state,
        userRemoveFromCompanyLoading: false,
        userRemoveFromCompanyLoaded: false,
        error: action.error
      };

    case USER_DISABLE:
      return {
        ...state,
        userDisabling: true
      };
    case USER_DISABLE_SUCCESS:
      return {
        ...state,
        userDisabling: false,
        isUserDisabled: true,
        user: action.result
      };
    case USER_DISABLE_FAIL:
      return {
        ...state,
        userDisabling: false,
        isUserDisabled: false,
        errorUserDisabling: action.error
      };

    case USER_ENABLE:
      return {
        ...state,
        userEnabling: true
      };
    case USER_ENABLE_SUCCESS:
      return {
        ...state,
        userEnabling: false,
        isUserEnabled: true,
        user: action.result
      };
    case USER_ENABLE_FAIL:
      return {
        ...state,
        userEnabling: false,
        isUserEnabled: false,
        errorUserEnabling: action.error
      };

    default:
      return state;
  }
}

export function superUserCreate({email, name}) {
  return {
    types: [SUPER_USER_CREATE, SUPER_USER_CREATE_SUCCESS, SUPER_USER_CREATE_FAIL],
    promise: (client) => client.post(`${API_URL}/users`, {
      data: {
        email, name
      }
    })
  };
}

export function userCreate({email, name, companyId}) {
  return {
    types: [USER_CREATE, USER_CREATE_SUCCESS, USER_CREATE_FAIL],
    promise: (client) => client.post(`${API_URL}/companies/${companyId}/users`, {
      data: {
        email, name
      }
    })
  };
}

export function showUsers() {
  return {
    types: [USERS_LOAD, USERS_LOAD_SUCCESS, USERS_LOAD_FAILED],
    promise: (client) => client.get(`${API_URL}/users`)
  };
}

export function getUser(userId) {
  return {
    types: [SINGLE_USER_LOAD, SINGLE_USER_LOAD_SUCCESS, SINGLE_USER_LOAD_FAILED],
    promise: (client) => client.get(`${API_URL}/users/${userId}`)
  };
}

export function listCompanies(id) {
  return {
    types: [USER_COMPANIES, USER_COMPANIES_SUCCESS, USER_COMPANIES_FAILED],
    promise: (client) => client.get(`${API_URL}/users/${id}/companies`)
  };
}

export function updateUser({userId, name}) {
  return {
    types: [SINGLE_USER_UPDATE, SINGLE_USER_UPDATE_SUCCESS, SINGLE_USER_UPDATE_FAILED],
    promise: (client) => client.put(`${API_URL}/users/${userId}`, {
      data: {
        name
      }
    })
  }
}

export function addUserToCompany(companyId, userId) {
  return {
    types: [USER_ADD_TO_COMPANY, USER_ADD_TO_COMPANY_SUCCESS, USER_ADD_TO_COMPANY_FAILED],
    promise: (client) => client.post(`${API_URL}/companies/${companyId}/users/${userId}`)
  }
}

export function userRemoveFromCompany(companyId, userId) {
  return {
    types: [USER_REMOVE_FROM_COMPANY, USER_REMOVE_FROM_COMPANY_SUCCESS, USER_REMOVE_FROM_COMPANY_FAILED],
    promise: (client) => client.del(`${API_URL}/companies/${companyId}/users/${userId}`)
  };
}

export function toggleUserSettingsBlock() {
  return {
    type: USER_TOGGLE_SETTINGS_BLOCK
  };
}

export function blockUser(userId) {
  return {
    types: [USER_DISABLE, USER_DISABLE_SUCCESS, USER_DISABLE_FAIL],
    promise: (client) => client.get(`${API_URL}/users/${userId}/block`)
  };
}

export function unblockUser(userId) {
  return {
    types: [USER_ENABLE, USER_ENABLE_SUCCESS, USER_ENABLE_FAIL],
    promise: (client) => client.get(`${API_URL}/users/${userId}/unblock`)
  };
}

export function cleanUserCreateError() {
  return {
    type: USER_CREATE_CLEAN_ERROR
  };
}
