'use strict';

const SPINNER_VISIBLE = 'SPINNER_VISIBLE';
const SPINNER_INVISIBLE = 'SPINNER_INVISIBLE';
const SPINNER_DID_MOUNT = 'SPINNER_DID_MOUNT';

const initialState = {
  isVisible : false
};

export default function reducer(state = initialState, action = {}){
  switch (action.type) {
    case SPINNER_VISIBLE:
      return {
        ...state,
        isVisible : true
      };

    case SPINNER_INVISIBLE:
      return {
        ...state,
        isVisible : false
      };

    case SPINNER_DID_MOUNT:
          return {
            ...state,
            isDidMount : true
          };

    default:
      return state;
  }
}

export function showSpinner() {
  return {
    type: SPINNER_VISIBLE
  }
}

export function didMountSpinner() {
  return {
    type: SPINNER_DID_MOUNT
  }
}

export function hideSpinner() {
  return {
    type: SPINNER_INVISIBLE
  }
}
