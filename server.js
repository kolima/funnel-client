'use strict';

require('./server.babel');

/**
 * Module dependencies.
 */
var app = require('funnel-frontend-core').app;
var server = app.start();
