import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom/server';
import serialize from 'serialize-javascript';

export default class Html extends Component {
  static propTypes = {
    config: PropTypes.object,
    component: PropTypes.node,
    store: PropTypes.object
  };

  render() {
    const {config, component, store} = this.props;
    const content = component ? ReactDOM.renderToString(component) : '';

    return (
      <html lang="en-us">
      <head>
        <meta charSet="utf-8"/>
        <title>{config.title}</title>

        <meta httpEquiv="Content-type" content="text/html;charset=UTF-8"/>
        <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"/>
        <meta name="HandheldFriendly" content="true"/>

        <meta name="keywords" content={config.keywords}/>
        <meta name="description" content={config.description}/>

        <link rel="shortcut icon" href="/build/favicon.ico" type="image/x-icon"/>
        <link rel="icon" href="/build/favicon.ico" type="image/x-icon"/>


        <link rel="stylesheet" href={'/' + config.cssFile}/>
      </head>
      <body>
      <div id="root" className="wrapper" dangerouslySetInnerHTML={{__html: content}}/>
      <script dangerouslySetInnerHTML={{__html: `window.__data=${serialize(store.getState())};`}} charSet="UTF-8"/>
      <script src={'/' + config.jsFile} charSet="UTF-8"/>
      </body>
      </html>
    );
  }
}
